import json

from tornado.ioloop import IOLoop
from tornado.escape import json_decode
from tornado.web import (
    RequestHandler,
    Application,
    HTTPError
)

PERSONS = []


def arg_to_int(val):
    try:
        int(val)
    except ValueError:
        raise HTTPError(400)

    return int(val)


class PersonHandler(RequestHandler):
    def post(self):
        data = json_decode(self.request.body)
        name = data.get('name')
        x_pos = arg_to_int(data.get('x'))
        y_pos = arg_to_int(data.get('y'))
        if not name:
            raise HTTPError(400)

        PERSONS.append({
            'id': len(PERSONS) + 1,
            'name': str(name),
            'x': x_pos,
            'y': y_pos
        })

        self.write(json.dumps({'persons': PERSONS}))


class NeighborHandler(RequestHandler):
    def get(self):
        x_pos = arg_to_int(self.get_argument('x'))
        y_pos = arg_to_int(self.get_argument('y'))
        count = arg_to_int(self.get_argument('n'))
        if count > 100:
            raise HTTPError(400)

        for person in PERSONS:
            distance = (
                    (x_pos - person['x']) ** 2 +
                    (y_pos - person['y']) ** 2
            )
            person['distance'] = distance

        sorted_neighbors = sorted(
            PERSONS,
            key=lambda k: k['distance']
        )

        self.write({'neighbors': sorted_neighbors[:count]})


if __name__ == "__main__":
    application = Application([
        (r"/person", PersonHandler),
        (r"/neighbor", NeighborHandler),
    ])
    application.listen(8888)
    IOLoop.instance().start()
